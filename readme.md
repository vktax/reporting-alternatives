# Reporting Alternatives

## Objectives

* Evaluate alternatives building reports on Azure Cloud
* Compare options to assess pros and cons
* Build small prototypes to assess different options in practicality

## Candidates

* Self-managed SSRS VM connected to Sql Server whether running on a VM or DBaaS
* Power BI Embedded: https://azure.microsoft.com/en-us/services/power-bi-embedded/
* Power BI Pro & Premium: https://powerbi.microsoft.com/en-us/power-bi-pro/
* Azure Databricks: https://azure.microsoft.com/en-us/services/databricks/
* Synapse Analytics: https://azure.microsoft.com/en-us/services/synapse-analytics/#overview
* Analysis Services: https://azure.microsoft.com/en-us/services/analysis-services/#overview
* Data Catalog: https://azure.microsoft.com/en-us/services/data-catalog/
* A complete list of analytics services on Azure: https://azure.microsoft.com/en-us/services/#analytics

## Self-managed SSRS

### Pros

* A familiar product

### Cons

* A Self-managed server as liability

### Pricing

* Depends on the size of VM selected e.g.: Standard DS12 $454.12/month

## Power BI Embedded

### Pros

* Built for embedded dashboards and reports
* Compatible with ASP.Net Core

### Cons

* Not suitable for self-service analytics

### Pricing

* Starting at ~$941.9687/month

## Power BI Pro & Premium

### Pros

* Self-service analytics
* SSRS reports [can be migrated](https://docs.microsoft.com/en-us/power-bi/guidance/migrate-ssrs-reports-to-power-bi) to Power BI

### Cons

* Not sure can be used for embedded reports
* Relatively high monthly cost

### Pricing

* Starting at $12.80/user/month for Pro and ~$6,393.60/month for Premium

## Databricks

### Pros

* Big data and Machine Learning using Apache Spark with lots of community knowledge
* Supports Python, Scala, R, Java, and SQL

### Cons

* Not suitable for non technical users
* Not built for embedded reports
* No support for .Net

### Pricing

* Not trivial to understand: https://azure.microsoft.com/en-us/pricing/details/databricks/

## Synapse Analytics

### Pros

* Big data and Machine Learning using Apache Spark
* Data warehouse and data lake, the backend for data analysis
* Supports T-SQL, Python, Scala, Spark SQL, and .Net

### Cons

* Not suitable for non technical users
* Does not come with a web based report designer

### Pricing

* Starting at ~$1,243.407/month

## Analysis Services

### Pros

* SQL Server Analysis Services on the cloud
* Modern data exploration and visualization tools like Power BI, Excel, and Reporting Service

### Cons

* Not suitable for non technical users
* Not built for embedded reports

### Pricing

* Starting at ~$401.792/month

## Data Catalog

### Pros

* Enterprise level data catalog and registry
* Self service Business Intelligence

### Cons

* Not built for embedded reports

### Pricing

* $1.28 per user per month